package sbu.cs;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.lang.*;
public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */

    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0) {
                sum += arr[i];
            }
        }
        return sum;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public int[] reverseArray(int[] arr) {
        int j = 0;
        int[] arr1 = new int[arr.length];
        for (int i = (arr.length) - 1; i >= 0 && j < arr.length; j++, i--) {
            arr1[j] = arr[i];
        }
        return arr1;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int row1, col1, row2, col2;
        row1 = m1.length;
        col1 = m1[0].length;
        row2 = m2.length;
        col2 = m2[0].length;
        double[][] product = new double[row1][col2];
        if (col1 != row2) {
            throw new RuntimeException();
        } else {

            double prod[][] = new double[row1][col2];

            for (int i = 0; i < row1; i++) {
                for (int j = 0; j < col2; j++) {
                    for (int k = 0; k < row2; k++) {
                        prod[i][j] = prod[i][j] + m1[i][k] * m2[k][j];
                    }
                }
            }

            for (int i = 0; i < row1; i++) {
                for (int j = 0; j < col2; j++) {
                    product[i][j] = prod[i][j];
                }
            }
        }
        return product;
    }


    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {

        List<List<String>> list2 = new ArrayList<>();
        int size1 = names.length;
        int size2 = names[0].length;
        for (int i = 0; i < size1; i++) {
            List<String> list1 = new ArrayList<>();
            for (int j = 0; j < size2; j++) {
                list1.add(names[i][j]);
            }
            list2.add(list1);
            // list1.clear();
        }
        return list2;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public List<Integer> primeFactors(int n) {
        List<Integer> list1 = new ArrayList<>();
        for (int i = 2; i <= n / 2; i++) {
            while (n % i == 0) {
                if(!list1.contains(i)){
                    list1.add(i);}
                n /= i;
            }
        }
        if (n > 1) {
            list1.add(n);
        }

        return list1;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
        List<String> list = new ArrayList<>();

        line=line.replace("!", " ");
        line=line.replace(",", " ");
        line=line.replace("?", " ");
        line=line.replace("  ", " ");
        String[] word=line.split(" ");
        int size = word.length;
        for (int i = 0; i < size; i++) {
            list.add(word[i]);

        }
        return list;
    }
}

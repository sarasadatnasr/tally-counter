package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private int value;

    public TallyCounter()
    {
        value = 0;
    }
    @Override
    public void count() {
        if (value >=0 && value <9999) {
            value = value + 1;
        }
    }
//some change just for adding it
    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void setValue(int max) throws IllegalValueException {
        {
            if(max>=0 && max<=9999){
                value=max;
            }
            else{
                throw new IllegalValueException();
            }
        }
    }
}

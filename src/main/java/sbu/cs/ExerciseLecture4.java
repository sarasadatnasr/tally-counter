package sbu.cs;
import java.lang.*;
import java.util.Locale;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int fact = 1;
        int i = 1;
        for (i = 1; i <= n; i++) {
            fact *= i;
        }
        return fact;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        int i = 2;
        long n1 = 1, n2 = 1, n3 = 1, count = 10;
        if (n == 1 || n == 2) {
            n3 = 1;
        } else {
            for (i = 2; i < n; ++i) {

                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
            }
        }
        return n3;

    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */


    public String reverse(String word) {
        {
            final StringBuilder builder = new StringBuilder(word);
            int length = builder.length();
            for (int i = 0; i < length / 2; i++) {
                final char current = builder.charAt(i);
                final int otherEnd = length - i - 1;
                builder.setCharAt(i, builder.charAt(otherEnd)); // swap
                builder.setCharAt(otherEnd, current);
            }
            return builder.toString();
        }
    }    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */

    public boolean isPalindrome(String line) {
        line = line.replace(" ", "");
        line = line.toLowerCase();
        int size = line.length();
        String revereLine=reverse(line);
        boolean isPalindrome = false;
        if(line.equals(revereLine)){
            isPalindrome=true;
            }
        return isPalindrome;


    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2) {
        int size1 = str1.length();
        int size2 = str2.length();
        char[][] letter = new char[size1][size2];
        for (int i = 0; i < size1; i++) {
            for (int j = 0; j < size2; j++) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    letter[i][j] = '*';
                } else {
                    letter[i][j] = ' ';
                }
            }
        }
        return letter;
    }
}
package sbu.cs;
import java.util.Random;
import java.lang.*;
public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length) {
        Random rand = new Random();
        String secret = "";
        String letter;
        for (int i = 0; i < length; i++) {
            Random r = new Random();
            char ch = (char) (r.nextInt(26) + 'a');
            secret = secret.concat(Character.toString(ch));
        }
        return secret;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception {
        Random position = new Random();
        String secret="";
     if (length >= 3 ) {
          int num = position.nextInt(length);
          int sch = position.nextInt(length);
          while(num==sch){
               sch = position.nextInt(length);
          }
          for (int i = 0; i < length; i++) {
            if (i!=num && i != sch) {
                 char ch = randomChar();
                 secret = secret.concat(Character.toString(ch));
            }
            else if (i==num){
                int num1 = randomInt();
                String number = String.valueOf(num1);
                secret = secret+num1 ;
            }
            else if (i==sch) {
                char special = randomSchar();
                String Specialchar=Character.toString(special);
                secret =secret+special;
            }
          }
     return secret;
     }
     else{
          throw new RuntimeException();
    }
    }


    public static char randomChar() {
        Random r = new Random();
        char ch = (char) (r.nextInt(26) + 'a');
        return ch;
    }
    public static int randomInt(){
        Random r = new Random();
        int num = r.nextInt(10);
        return num;
    }
    public static char randomSchar(){
        Random r = new Random();
        char sch = (char) (r.nextInt(14) + ' ');
        return sch;
    }


    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n) {
        boolean isfibibin=false;
        for(int i=1 ;i<=n ;i++){
            if(n==fibonacci(i)+binfibonacci(i)){
                isfibibin=true;
            }
        }
        return isfibibin;
    }

    public long fibonacci(int n) {
        int i = 2;
        long n1 = 1, n2 = 1, n3 = 1, count = 10;
        if (n == 1 || n == 2) {
            n3 = 1;
        } else {
            for (i = 3; i < n; ++i) {

                n3 = n1 + n2;
                n1 = n2;
                n2 = n3;
            }
        }
        return n3;

    }
    public long binfibonacci(int n) {
        long one = 0;
        long n1 = 1, n2 = 1, n3 = 1, count = 10;
        if (n == 1 || n == 2) {
            one = 1;
        } else {
            long fib = fibonacci(n);
            while (fib > 0) {
                if (fib % 2 == 1) {
                    one++;
                }
                fib = fib / 2;
            }
        }
        return one;
    }









}
